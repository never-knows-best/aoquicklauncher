﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.IO;
using Reloaded.Injector;
using CommandLine.Text;
using CommandLine;

namespace AOQuickLauncher
{
    public class Program
    {
        public class Options
        {
            [Value(0, MetaName = "Username", Required = true)]
            public string Username { get; set; }

            [Value(1, MetaName = "Password", Required = true)]
            public string Password { get; set; }

            [Value(2, MetaName="Character ID", Required = true)]
            public int CharacterId { get; set; }

            [Option('q', "quiet", Required = false, HelpText = "Quiet Mode")]
            public bool Quiet { get; set; }

            [Option('x', "nochat", Required = false, HelpText = "Client will not connect to chat server")]
            public bool NoChat { get; set; }

            [Option('s', "rk19", Required = false, HelpText = "Connect to RK19")]
            public bool Rubika19 { get; set; }
        }

        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed(SpawnAOProcess).WithNotParsed((e) =>
            {
                Console.WriteLine();
            });
        }

        private static void SpawnAOProcess(Options options)
        {
            string? exportedPath = Environment.GetEnvironmentVariable("AOPath");

            string aoPath = exportedPath != null ? exportedPath : AppContext.BaseDirectory;

            Process client = new Process();
            client.StartInfo.WorkingDirectory = aoPath;
            client.StartInfo.FileName = $"{aoPath}\\AnarchyOnline.exe";
            client.StartInfo.Arguments = options.Rubika19 ? "IA700453413 IP7506 DU" : "IA700453413 IP7505 DU";
            client.StartInfo.EnvironmentVariables.Add("AOUser", options.Username);
            client.StartInfo.EnvironmentVariables.Add("AOPass", options.Password);
            client.StartInfo.EnvironmentVariables.Add("AOCharId", options.CharacterId.ToString());
            client.StartInfo.EnvironmentVariables.Add("QuietLaunch", Convert.ToString(options.Quiet));
            client.StartInfo.EnvironmentVariables.Add("NoChat", Convert.ToString(options.NoChat));
            client.Start();

            Injector injector = new Injector(client);
            injector.Inject($"{AppContext.BaseDirectory}\\LoginHandler.dll");
        }
    }
}
